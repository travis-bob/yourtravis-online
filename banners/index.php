<?
// https://gist.github.com/taterbase/2688850

$allow = array(
  "77.60.83.154",
  "77.60.83.155",
  "213.127.118.43"
); //allowed IPs


if(!in_array($_SERVER['REMOTE_ADDR'], $allow) && !in_array($_SERVER["HTTP_X_FORWARDED_FOR"], $allow) && !in_array($_SERVER["HTTP_CLIENT_IP"], $allow))  {
  echo 'no access';
  die;
}

if($_REQUEST['deletefile']){
    $filename=$_REQUEST['deletefile'];
    unlink($filename);
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>TRAVIS banner upload</title>
  <style>
    img {max-width: 600px;}
    td {border: 1px solid grey;}
    table, tr, td { margin: 0; padding: 0;}
    .fontawesome {font-family: FontAwesome; color: black;}
    .fontawesome:after {font-size: 1em; display:block; padding: 0 10px; }
    .trash:after {content: "\f1f8"; }
    .linkout:after {content: "\f0c1"; }
  </style>
  <script src="https://use.fontawesome.com/3153295458.js"></script>
</head>
<body>
  <form enctype="multipart/form-data" action="index.php" method="POST">
    <b>Bestanden uploaden</b>
    <br /><br />
    <input type="file" name="uploaded_file"></input><br />
    <input type="submit" value="Upload"></input>
  </form>
  <br /><br />
  <b>Bestanden</b>
  <br /><br />
  <table>
  <?
  $fileList = glob('banners/*');
  foreach($fileList as $filename){
      //Use the is_file function to make sure that it is not a directory.
      if(is_file($filename) && ($filename != 'index.php') ){

        list($width, $height, $type, $attr) = getimagesize($filename);

        echo '<tr>';

        echo '<td><img src="' . $filename . '"></td>';
        echo '<td>width: ' . $width . '<br />height: ' . $height . '</td>';
        echo '<td>' . str_replace( 'banners/', '', $filename ) . '</td>';
        echo '<td><a class="fontawesome linkout" href="https://banners.yourtravis.online/' . $filename . '" target="_blank"></td>';
        //echo '<td><a class="fontawesome trash" href="#" onClick="if(confirm(\'Are you sure?\')){ window.location=\'index.php?deletefile=' . $filename . '\'; }" id="' . $filename . '"></a></td>';

        echo '</tr>';
      }
  }
    ?>
  </table>
</body>
</html>

<?PHP
  if(!empty($_FILES['uploaded_file']))
  {
    $path = dirname();
    $path = $path . 'banners/' . basename( $_FILES['uploaded_file']['name']);

    if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
      echo "The file ".  basename( $_FILES['uploaded_file']['name']).
      " has been uploaded. PLEASE WAIT";
      echo "<meta http-equiv='refresh' content='1'>";
    } else{
        echo "There was an error uploading the file, please try again!";
    }
  }
?>
