<?php
// https://www.ternstyle.us/blog/google-analytics-tracking-in-php

  $matchFound = ( array_key_exists("campaign", $_GET) && array_key_exists("website", $_GET) && array_key_exists("banner", $_GET) );

  if (!$matchFound) {
    echo 'no access';
    die;
  }


  // Get parameters from url
  $campaign       = $_GET["campaign"];
  $source         = $_GET["source"];
  $banner         = $_GET["banner"];



  //some of the functions we need to make it work
	function generate_serial($n) {
		$c = "abcdefghijklmnopqrstuvwyxz0123456789";
		$s = '';
		for($i=0;$i<$n;$i++) {
			$s .= substr($c,rand(0,37),1);
		}
		return $s;
	}
	function generate_uuid() {
		return generate_serial(8).'-'.generate_serial(4).'-4'.generate_serial(3).'-a'.generate_serial(3).'-'.generate_serial(12);
	}

		$ip = false;
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = explode(',',$_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip = trim(array_shift($ip));
		}
		elseif(isset($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

	//create a UUID string for the user sending the request and store in the the session
	if(isset($_COOKIE['Google_Analytics_UUID']) and !empty($_COOKIE['Google_Analytics_UUID'])) {
		$uuid = $_COOKIE['Google_Analytics_UUID'];
	}
	else {
		$uuid = generate_uuid();
		setcookie('Google_Analytics_UUID', $$uuid ,time()+63072000);
	}


  $ua_url       = 'https://www.google-analytics.com/collect';
  // event in UA
  $ua_parameters = array (
    'v' => '1',
    'tid'		=>	'UA-163818224-2',	// Tracking ID / Web Property ID
		'cid'		=>	$uuid,	// Client ID
		'uip'		=>	$ip,	// IP Override
    't' => 'event',
    'ec' => 'banner-counter', // category
    'ea' => $campaign, // action
    'el' => $source, // label
    //'ev' => 'yyy', // value
  );

  //send using PHP's cURL extension
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $ua_url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($ua_parameters));
  //curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	$response = curl_exec($ch);

	//parse the response and send back to the browser
	//header('Content-Type: application/json');
	$status = curl_getinfo($ch,CURLINFO_HTTP_CODE).PHP_EOL;
  file_put_contents('./log_'.date("j.n.Y").'.log', $status, FILE_APPEND);

  //echo $ua_url . '?' . http_build_query($ua_parameters);

  // Return the image
  $image = file_get_contents('banners/' . $banner);
  header('content-type: image/png');
  echo $image;
?>
