<?php

header('Content-Type: text/xml');

$matchFound = (array_key_exists("id", $_GET) && trim($_GET["id"]) == 'gToS6EiMHZ4VUJ');

if (!$matchFound) {
  echo 'no access';
  die;
}

else {


function array2xml($array, $xml = false){

    if($xml === false){
        $xml = new SimpleXMLElement('<items/>');
    }

    foreach($array as $key => $value){
        if(is_array($value)){
            array2xml($value, $xml->addChild(is_numeric((string) $key)?("partnervalue".$key):$key));
        } else {
            $xml->addChild(is_numeric((string) $key)?("n".$key):$key, $value);
        }
    }

    return $xml->asXML();
}

$raw_data = file_get_contents('https://api.yourtravis.com/?v=2.0&component=locations&key=gToS6EiMHZ4VUJy54Omg&action=getlist');
$jSON = json_decode($raw_data, true);

$xml = array2xml($jSON, false);
$xml = str_replace('<?xml version="1.0"?>', '<?xml version=\'1.0\' encoding=\'UTF-8\'?>', $xml);
$xml = preg_replace('/<partnervalue(\d+)>/', '<item>', $xml);
$xml = str_replace('</partnervalue', '<0partnervalue', $xml);
$xml = preg_replace('/<0partnervalue(\d+)>/', '</item>', $xml);
$xml = str_replace('24_hours_service', 'x24_hours_service', $xml);

//echo '<pre>';
print_r($xml);
//print_r($jSON);


}
